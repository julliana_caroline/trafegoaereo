package enactors;

import context.arch.discoverer.ComponentDescription;
import context.arch.discoverer.component.NonConstantAttributeElement;
import context.arch.discoverer.query.AbstractQueryItem;
import context.arch.discoverer.query.ElseQueryItem;
import context.arch.discoverer.query.ANDQueryItem;
import context.arch.discoverer.query.RuleQueryItem;
import context.arch.discoverer.query.comparison.AttributeComparison;
import context.arch.enactor.Enactor;
import context.arch.enactor.EnactorReference;
import context.arch.service.helper.ServiceInput;
import context.arch.storage.AttributeNameValue;
import context.arch.storage.Attributes;
import context.arch.widget.Widget;
import context.arch.widget.Widget.WidgetData;
import widgets.AlarmeWidget;

public class AeronaveEnactorAlerta extends Enactor {

    public static int Y_1 = 0;
    public static int Y_2 = 0;

    @SuppressWarnings("serial")
    public AeronaveEnactorAlerta(AbstractQueryItem<?, ?>[] inWidgetQuery, AbstractQueryItem<?, ?>[] outWidgetQuery) {
        super(inWidgetQuery, outWidgetQuery, "aeronave", "");

        AbstractQueryItem<?, ?> alarmeAeronave
                = new ANDQueryItem(
                        RuleQueryItem.instance(
                                new NonConstantAttributeElement(AttributeNameValue.instance("Y_1", Y_1)),
                                new AttributeComparison(AttributeComparison.Comparison.EQUAL)),
                        RuleQueryItem.instance(
                                new NonConstantAttributeElement(AttributeNameValue.instance("Y_2", Y_2)),
                                new AttributeComparison(AttributeComparison.Comparison.EQUAL))
                );

        EnactorReference er = new AeronaveEnactorReference(alarmeAeronave, AlarmeWidget.ALARME_ON);
        er.addServiceInput(new ServiceInput("AeronaveService", "aeronaveControl",
                new Attributes() {
            {
                addAttribute("Y_1", Integer.class);
                addAttribute("Y_2", Integer.class);
            }
        }));
        addReference(er);

        er = new AeronaveEnactorReference(new ElseQueryItem(alarmeAeronave), AlarmeWidget.ALARME_OFF);
        er.addServiceInput(new ServiceInput("AeronaveService", "aeronaveControl",
                new Attributes() {
            {
                addAttribute("Y_1", Integer.class);
                addAttribute("Y_2", Integer.class);

            }
        }));
        addReference(er);

        start();
    }

    private class AeronaveEnactorReference extends EnactorReference {

        public AeronaveEnactorReference(AbstractQueryItem<?, ?> conditionQuery, String outcomeValue) {
            super(AeronaveEnactorAlerta.this, conditionQuery, outcomeValue);
        }

        @Override
        protected Attributes conditionSatisfied(ComponentDescription inWidgetState, Attributes outAtts) {
            long timestamp = outAtts.getAttributeValue(Widget.TIMESTAMP);
            WidgetData data = new WidgetData("AeronaveWidget", timestamp);
            String alarmeLigado = "";
            String alarmeDesligado = "";
            if (outcomeValue.equals(AlarmeWidget.ALARME_ON)) {
                alarmeLigado = inWidgetState.getAttributeValue(AlarmeWidget.ALARME_ON);
            } else {
                alarmeDesligado = inWidgetState.getAttributeValue(AlarmeWidget.ALARME_OFF);
            }

            data.setAttributeValue(AlarmeWidget.ALARME_ON, alarmeLigado);
            data.setAttributeValue(AlarmeWidget.ALARME_OFF, alarmeDesligado);
            outAtts.putAll(data.toAttributes());

            return outAtts;
        }
    }
}

package enactors;

import context.arch.discoverer.ComponentDescription;
import context.arch.discoverer.component.NonConstantAttributeElement;
import context.arch.discoverer.query.ANDQueryItem;
import context.arch.discoverer.query.AbstractQueryItem;
import context.arch.discoverer.query.ElseQueryItem;
import context.arch.discoverer.query.RuleQueryItem;
import context.arch.discoverer.query.comparison.AttributeComparison;
import context.arch.enactor.Enactor;
import context.arch.enactor.EnactorReference;
import context.arch.service.helper.ServiceInput;
import context.arch.storage.AttributeNameValue;
import context.arch.storage.Attributes;
import context.arch.widget.Widget;
import context.arch.widget.Widget.WidgetData;
import widgets.AeronaveWidget;
import widgets.AlarmeWidget;

public class AeronaveEnactorDesvio extends Enactor {

    public static final String RESULTADO = "true";
    public static final String ALARME_ON = "true";
    public static final String ALARME_OFF = "false";
    

    @SuppressWarnings("serial")
    public AeronaveEnactorDesvio(AbstractQueryItem<?, ?>[] inWidgetQuery, AbstractQueryItem<?, ?>[] outWidgetQuery) {
        super(inWidgetQuery, outWidgetQuery, "aeronave", "");

        AbstractQueryItem<?, ?> alarmeAeronave
                = new ANDQueryItem(
                        RuleQueryItem.instance(
                                new NonConstantAttributeElement(AttributeNameValue.instance("RESULTADO", RESULTADO)),
                                new AttributeComparison(AttributeComparison.Comparison.EQUAL)),
                        RuleQueryItem.instance(
                                new NonConstantAttributeElement(AttributeNameValue.instance("ALARME_ON", ALARME_ON)),
                                new AttributeComparison(AttributeComparison.Comparison.EQUAL))
                );

        EnactorReference er = new AeronaveEnactorReference(alarmeAeronave, AlarmeWidget.ALARME_ON);
        er.addServiceInput(new ServiceInput("AeronaveService", "aeronaveControl",
                new Attributes() {
            {
                addAttribute("ALARME_ON", Integer.class);
                addAttribute("RESULTADO", Integer.class);
                addAttribute("ALARME_OFF", Integer.class);
            }
        }));
        addReference(er);

        er = new AeronaveEnactorReference(new ElseQueryItem(alarmeAeronave), AlarmeWidget.ALARME_OFF);
        er.addServiceInput(new ServiceInput("AeronaveService", "aeronaveControl",
                new Attributes() {
            {
                addAttribute("ALARME_ON", Integer.class);
                addAttribute("RESULTADO", Integer.class);
                addAttribute("ALARME_OFF", Integer.class);
               
            }
        }));
        addReference(er);

        start();
    }

    private class AeronaveEnactorReference extends EnactorReference {

        public AeronaveEnactorReference(AbstractQueryItem<?, ?> conditionQuery, String outcomeValue) {
            super(AeronaveEnactorDesvio.this, conditionQuery, outcomeValue);
        }

        @Override
        protected Attributes conditionSatisfied(ComponentDescription inWidgetState, Attributes outAtts) {
            long timestamp = outAtts.getAttributeValue(Widget.TIMESTAMP);
            WidgetData data = new WidgetData("AeronaveWidget", timestamp); 
            int desvioY = 0;
            if (outcomeValue.equals(AlarmeWidget.ALARME_ON)) {
                int y_1 = inWidgetState.getAttributeValue(AeronaveWidget.Y_1);
                desvioY = y_1 - 2;
            } else {
                desvioY = 0;
            }

            data.setAttributeValue(AeronaveWidget.Y_1, desvioY);
            outAtts.putAll(data.toAttributes());

            return outAtts;
        }

    }

}

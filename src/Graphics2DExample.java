
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class Graphics2DExample extends JComponent {
//  private static Color m_tRed = new Color(255, 0, 0, 150);
//
//  private static Color m_tGreen = new Color(0, 255, 0, 150);
//
//  private static Color m_tBlue = new Color(0, 0, 255, 150);
//
//  private static Font monoFont = new Font("Monospaced", Font.BOLD
//      | Font.ITALIC, 36);
//
//  private static Font sanSerifFont = new Font("SanSerif", Font.PLAIN, 12);
//
//  private static Font serifFont = new Font("Serif", Font.BOLD, 24);
//

    private final ImageIcon a1 = new ImageIcon("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\TrafegoAereo\\img\\a1.png");
    private final ImageIcon a2 = new ImageIcon("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\TrafegoAereo\\img\\a2.png");
    private final ImageIcon a3 = new ImageIcon("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\TrafegoAereo\\img\\a3.png");

    private int x1, y1, x2, y2, x3, y3;

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // draw entire component white
        g.setColor(Color.white);
        g.fillRect(0, 0, getWidth(), getHeight());

        // yellow circle
        //g.setColor(Color.yellow);
        //g.fillOval(x1, y1, 20, 20);
        // magenta circle
        //g.setColor(Color.magenta);
        //g.fillOval(x2, y2, 20, 20);
        // paint the icon below blue sqaure
        a1.paintIcon(this, g, x1, y1);
        //a2.paintIcon(this, g, x2, y2);
        a3.paintIcon(this, g, x3, y3);

        // paint the icon below red sqaure
//    java2sLogo.paintIcon(this, g, 120 - (w / 2), 280 - (h / 2));
//
//    // transparent red square
//    g.setColor(m_tRed);
//    g.fillRect(60, 220, 120, 120);
//
//    // transparent green circle
//    g.setColor(m_tGreen);
//    g.fillOval(140, 140, 120, 120);
//
//    // transparent blue square
//    g.setColor(m_tBlue);
//    g.fillRect(220, 60, 120, 120);
//
//    g.setColor(Color.black);
//
//    g.setFont(monoFont);
//    FontMetrics fm = g.getFontMetrics();
//    w = fm.stringWidth("Java Source");
//    h = fm.getAscent();
//    g.drawString("Java Source", 120 - (w / 2), 120 + (h / 4));
//
//    g.setFont(sanSerifFont);
//    fm = g.getFontMetrics();
//    w = fm.stringWidth("and");
//    h = fm.getAscent();
//    g.drawString("and", 200 - (w / 2), 200 + (h / 4));
//
//    g.setFont(serifFont);
//    fm = g.getFontMetrics();
//    w = fm.stringWidth("Support.");
//    h = fm.getAscent();
//    g.drawString("Support.", 280 - (w / 2), 280 + (h / 4));
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(400, 400);
    }

    @Override
    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getX3() {
        return x3;
    }

    public void setX3(int x3) {
        this.x3 = x3;
    }

    public int getY3() {
        return y3;
    }

    public void setY3(int y3) {
        this.y3 = y3;
    }

    public static void main(String args[]) throws InterruptedException {
        JFrame mainFrame = new JFrame("Graphics demo");
        Graphics2DExample tela = new Graphics2DExample();
        tela.setX1(0);
        tela.setY1(350);
        
        tela.setX2(368);
        tela.setY2(350);
        mainFrame.getContentPane().add(tela);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
        int cont = 0;
        
        while (true) {
            
            if((Math.abs(tela.getX1() - tela.getX3()) < 80) && (Math.abs(tela.getY1() - tela.getY3()) < 80)) {
                tela.setX1(tela.getX1() + 3);
            } else {
                tela.setX1(tela.getX1() + 1);
                tela.setY1(tela.getY1() - 1);
            }
            //tela.setX2(tela.getX2() - 1);
            //tela.setY2(tela.getY2() - 1);
            
            tela.setX3(tela.getX3() + 1);
            tela.setY3(tela.getY3() + 1);

            tela.repaint();

            Thread.sleep(17);
        }
    }
}

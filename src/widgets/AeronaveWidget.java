package widgets;

import context.arch.storage.Attribute;
import context.arch.storage.AttributeNameValue;
import context.arch.widget.Widget;

/**
 *
 * @author Julliana Caroline
 */
public class AeronaveWidget extends Widget {

    public static final String AERONAVE = "aeronave";
    public static final String X = "x";
    public static final String Y_1 = "y1";
    public static final String Y_2 = "y2";
    public static final String X_VEL = "x_vel";
    public static final String Y_VEL = "y_vel";

    private final String aeronave;

    public AeronaveWidget(String aeronave) {
        super(AeronaveWidget.class.getName(), AeronaveWidget.class.getName());
        this.aeronave = aeronave;
        super.start(true);
    }

    @Override
    protected void init() {
        // non-constant attributes
        addAttribute(Attribute.instance(X, Integer.class));
        addAttribute(Attribute.instance(Y_1, Integer.class));
        addAttribute(Attribute.instance(Y_2, Integer.class));
        addAttribute(Attribute.instance(X_VEL, Integer.class));
        addAttribute(Attribute.instance(Y_VEL, Integer.class));

        // constant attributes
        addAttribute(AttributeNameValue.instance(AERONAVE, aeronave), true);
    }
}

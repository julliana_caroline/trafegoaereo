package widgets;

import context.arch.storage.Attribute;
import context.arch.storage.AttributeNameValue;
import context.arch.widget.Widget;

public class AlarmeWidget extends Widget {

    public static final String ALARME_ON = "On";
    public static final String ALARME_OFF = "Off";

    private String aeronave;

    public AlarmeWidget(String aeronave) {
        super(AlarmeWidget.class.getName(), AlarmeWidget.class.getName());
        this.aeronave = aeronave;
        super.start(true);
    }

    @Override
    protected void init() {
        // non-constant attributes
        addAttribute(Attribute.instance(ALARME_ON, Boolean.class));
        addAttribute(Attribute.instance(ALARME_OFF, Boolean.class));

        // constant attributes
        addAttribute(AttributeNameValue.instance("aeronave", aeronave), true);
    }
}

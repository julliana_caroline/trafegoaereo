
import java.awt.Dimension;
import javax.swing.JFrame;
import context.arch.discoverer.Discoverer;
import model.Espaco;

/**
 *
 * @author Julliana Caroline
 */
public class Main {

	public static void main(String[] args) {
		Discoverer.start();
		
		Espaco app = new Espaco("RN");
		
		JFrame frame = new JFrame("Apresentação - Julliana");
		frame.add(app.ui);   
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(400, 400));
		frame.setLocationRelativeTo(null); // center of screen
		frame.setVisible(true);
	}

}

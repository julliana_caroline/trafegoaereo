package model;

import context.arch.discoverer.query.AbstractQueryItem;
import context.arch.widget.WidgetXmlParser;
import enactors.AeronaveEnactorAlerta;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import services.AeronaveService;
import widgets.AeronaveWidget;
import widgets.AlarmeWidget;

/**
 *
 * @author Julliana Caroline
 */
public class Aeronave {
    
    public AeronaveWidget aeronaveWidget;
    public AeronaveService aeronaveService;
    public AlarmeWidget alarmeWidget;
    public AeronaveEnactorAlerta alarmeEnactor;

    public String aeronave;
    public AeronaveUI ui;

    public Aeronave(String aeronave) {
        this.aeronave = aeronave;
        
        this.ui = new AeronaveUI();
        
        aeronaveWidget = new AeronaveWidget(aeronave);
        aeronaveService = new AeronaveService(aeronaveWidget, aeronave, ui);
        aeronaveWidget.addService(aeronaveService);
        
        alarmeWidget = new AlarmeWidget(aeronave);
        
        AbstractQueryItem<?,?>[] inAeronaveWidgetQuery = {WidgetXmlParser.createWidgetSubscriptionQuery(aeronaveWidget)};
        AbstractQueryItem<?,?>[] outAeronaveWidgetQuery = {WidgetXmlParser.createWidgetSubscriptionQuery(alarmeWidget)};
        alarmeEnactor = new AeronaveEnactorAlerta(inAeronaveWidgetQuery, outAeronaveWidgetQuery);
        
        aeronaveWidget.updateData(AeronaveWidget.X, 0);
        aeronaveWidget.updateData(AeronaveWidget.Y_1, 0);
        aeronaveWidget.updateData(AeronaveWidget.X_VEL, 0);
        aeronaveWidget.updateData(AeronaveWidget.Y_VEL, 0);
    }

    public class AeronaveUI extends JPanel {

        private BufferedImage aeronaveImage;

        public AeronaveUI() {
            try {
                switch (aeronave) {
                    case "1":
                        aeronaveImage = ImageIO.read(new File("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\Exemplo\\src\\img\\aeronave1.png"));
                        break;
                    case "2":
                        aeronaveImage = ImageIO.read(new File("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\Exemplo\\src\\img\\aeronave2.png"));
                        break;
                    case "3":
                        aeronaveImage = ImageIO.read(new File("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\Exemplo\\src\\img\\aeronave3.png"));
                        break;
                }
                this.setSize(aeronaveImage.getWidth(), aeronaveImage.getHeight());
            } catch (IOException e) {
                System.out.println(e);
            }

            try {
                new Controler();
            } catch (IOException ex) {
                System.out.println(ex);
            }
            new Thread() {
                @Override
                public void run() {
                    while (true) {
                        repaint();
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            System.out.println(e);
                        }
                    }
                }
            }.start();
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics = (Graphics2D) g;
            int w = aeronaveImage.getWidth();
            int h = aeronaveImage.getHeight();

            setBackground(Color.WHITE);

            graphics.drawImage(aeronaveImage, 0, 0, w, h, null);
            graphics.dispose();
        }
    }

    public class Controler extends JFrame {

        private float fontSize = 10f;
        private JSpinner x_pos;
        private JSpinner y_pos;
        private JSlider x_vel;
        private JSlider y_vel;

        public Controler() throws IOException {
            setTitle("Controlador da aeronave " + aeronave);
            setSize(new Dimension(330, 200));
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(4, 2));

            panel.add(new JLabel("Eixo X") {
                {
                    setFont(getFont().deriveFont(fontSize));
                }
            });

            panel.add(x_pos = new JSpinner(new SpinnerNumberModel(0, 0, 400, 1)) {
                {
                    setFont(getFont().deriveFont(fontSize));
                    addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent evt) {
                            int x = (Integer) x_pos.getValue();
                            //System.out.println("Eixo x: " + x);
                            aeronaveWidget.updateData(AeronaveWidget.X, x);
                        }
                    });
                }
            });

            panel.add(new JLabel("Eixo Y") {
                {
                    setFont(getFont().deriveFont(fontSize));
                }
            });

            panel.add(y_pos = new JSpinner(new SpinnerNumberModel(0, 0, 400, 1)) {
                {
                    setFont(getFont().deriveFont(fontSize));
                    addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent evt) {
                            int y = (Integer) y_pos.getValue();
                            //System.out.println("Eixo y: " + y);
                            aeronaveWidget.updateData(AeronaveWidget.Y_1, y);
                        }
                    });
                }
            });

            panel.add(new JLabel("Velocidade em X") {
                {
                    setFont(getFont().deriveFont(fontSize));
                }
            });

            panel.add(x_vel = new JSlider(new DefaultBoundedRangeModel(0, 0, 0, 10)) {
                {
                    addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent evt) {
                            int vel = x_vel.getValue();
                            //System.out.println("Velocidade em X: " + vel);
                            aeronaveWidget.updateData(AeronaveWidget.X_VEL, vel);
                        }
                    });
                }
            });

            panel.add(new JLabel("Velocidade em Y") {
                {
                    setFont(getFont().deriveFont(fontSize));
                }
            });

            panel.add(y_vel = new JSlider(new DefaultBoundedRangeModel(0, 0, 0, 10)) {
                {
                    addChangeListener(new ChangeListener() {
                        @Override
                        public void stateChanged(ChangeEvent evt) {
                            int vel = y_vel.getValue();
                            //System.out.println("Velocidade em Y: " + vel);
                            aeronaveWidget.updateData(AeronaveWidget.Y_VEL, vel);
                        }
                    });
                }
            });

            add(panel);
            setVisible(true);
        }
    }
}

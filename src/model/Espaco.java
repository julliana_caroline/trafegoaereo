package model;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Julliana Caroline
 */
public class Espaco {

    public String espaco;
    public Aeronave aeronave1;
    public Aeronave aeronave2;
    public Aeronave aeronave3;
    public EspacoUI ui;

    public Espaco(String espaco) {
        this.espaco = espaco;
        this.aeronave1 = new Aeronave("1");
        this.aeronave2 = new Aeronave("2");
        this.aeronave3 = new Aeronave("3");
        this.ui = new EspacoUI();
    }

    public class EspacoUI extends JPanel {

        BufferedImage espacoImage;

        public EspacoUI() {
            setLayout(null);
            try {
                espacoImage = ImageIO.read(new File("C:\\Users\\Julliana\\Documents\\NetBeansProjects\\Exemplo\\src\\img\\espaco.png"));
            } catch (IOException e) {
                System.out.println(e);
            }

            add(aeronave1.ui);
            aeronave1.ui.setLocation(0, 10);
            add(aeronave2.ui);
            aeronave2.ui.setLocation(0, 100);
            add(aeronave3.ui);
            aeronave3.ui.setLocation(0, 200);
            
            new Thread() {
                @Override
                public void run() {
                    while (true) {
                        repaint();
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            System.out.println(e);
                        }
                    }
                }
            }.start();
        }

        @Override  
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics = (Graphics2D) g;
            int w = espacoImage.getWidth();
            int h = espacoImage.getHeight();

            graphics.drawImage(espacoImage, 0, 0, w, h, null);
            graphics.dispose();
        }
    }
}

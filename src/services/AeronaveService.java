
package services;

import context.arch.comm.DataObject;
import context.arch.service.Service;
import context.arch.service.helper.FunctionDescription;
import context.arch.service.helper.FunctionDescriptions;
import context.arch.service.helper.ServiceInput;
import context.arch.widget.Widget;
import javax.swing.JPanel;
import widgets.AeronaveWidget;

/**
 *
 * @author Julliana Caroline
 */
public class AeronaveService  extends Service {
    public JPanel aeronavePanel;
    public String aeronave;

    @SuppressWarnings("serial")
    public AeronaveService(final Widget widget, String aeronave, JPanel aeronavePanel) {
        super(widget, "AeronaveService",
                new FunctionDescriptions() {
            {
                add(new FunctionDescription(
                        "aeronaveControle",
                        "Seta as coordenadas e velocidade do avião",
                        widget.getNonConstantAttributes()));
            }
        });
        
        this.aeronave = aeronave;
        this.aeronavePanel = aeronavePanel;
    }

    @Override
    public DataObject execute(ServiceInput serviceInput) {
        int x = serviceInput.getInput().getAttributeValue(AeronaveWidget.X);
        int y_1 = serviceInput.getInput().getAttributeValue(AeronaveWidget.Y_1);
        int x_vel = serviceInput.getInput().getAttributeValue(AeronaveWidget.X_VEL);
        int y_vel = serviceInput.getInput().getAttributeValue(AeronaveWidget.Y_VEL);
        
        aeronavePanel.setLocation(x + x_vel, y_1 + y_vel);
        
        return new DataObject();
    }
    
}
